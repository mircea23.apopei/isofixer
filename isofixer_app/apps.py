from django.apps import AppConfig


class IsofixerAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'isofixer_app'
