from django.shortcuts import render
from django.http import HttpResponse
from isofixer_app.models import *
from .forms import CreateNewList

# Create your views here.


# def index(response):
#     return HttpResponse("<h1>welcome to your ISO Fixer!</h1>")


from isofixer_app.models import ToDoList


# def index(response, name):  # A din isofixer_app/urls.py
#     ls: ToDoList = ToDoList.objects.get(name=name)
#     items = ls.car_set.get(id=1)
#     return HttpResponse(f"<h1>{ls.name}<p>{str(items.car_type)}</p></h1>")


# '''B din isofixer_app/urls.py - ASTA E BUSIT RAU!'''
# def v1(response, car_type):
#     ls = ToDoList.objects.get(car_type=car_type)
#     car = ls.car_set.get(id=1)
#     return HttpResponse(f"<h1>{{ls.car}} {{str(car.car_type}}</h1>")

# asta il facem pentru base si home and so on. linia 16 va fi inlocuita de functia asta.
# def index(response, id):  # A index pentru base.html - asta e un exemplu care va afisa numele listei de la poz indicata in url/poz indicata
#     ls = ToDoList.objects.get(id=id)
#     return render(response, "isofixer_app/base.html", {"name": ls.name})

def index(response, id):  # A index pentru base.html
    ls = ToDoList.objects.get(id=id)
    return render(response, "isofixer_app/list.html", {"ls": ls})


# pentru path-ul din isofixer_app\urls - linia 8 cream o functie noua:
def home(response):
    # return render(response, "isofixer_app/home.html", {"name": "test"}) - ne arata "test" pe home page (url-ul fara /ceva)
    return render(response, "isofixer_app/home.html", {})


def create(response):
    form = CreateNewList()
    return render(response, "isofixer_app/create.html", {'form':form})
