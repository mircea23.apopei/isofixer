from django.urls import path
from . import views  # importa fisierul views din directorul de _app; face legatura cu views

urlpatterns = [
    # path("", views.index, name="index"),
    # path("<str:name>", views.index, name="index"),  # A din isofixer_app/views.py pt linia 6
    path("<int:id>", views.index, name="index"),  # pentru base, home, and so on
    # path("v1/", views.v1, name="view 1"),  - B din isofixer_app/views.py
    path("", views.home, name="home"),
    path("create/", views.create, name="create"),

]
