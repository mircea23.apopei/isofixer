"""isofixer URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

# path-se refera la extensia pe care o primeste url-ul pentru a merge pe alte cai(paths), ex: home, sign-in, etc.
urlpatterns = [
    path('admin/', admin.site.urls),  # admin - e path-ul de admin
    path('', include("isofixer_app.urls")),  # leaga toate path-urile de fisierul de urls din _app

]
